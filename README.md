# Mitglieder
* Bastian Stamm 1584187
* Jörn Eggersglüß 1457773
* Diem-Mi Nguyen 1584174
* Julian Wendling 1584381

# Tools
[Figma](https://www.figma.com/)  - Wireframes  
[Postman](https://www.postman.com/)  - Testen der API Endpoints  
[Gitlab Repo](https://gitlab.com/joern.e/steam_wt)  

# Aufgaben
Die Aufgaben wurden immer zusammen über Discord mit Screenshare bearbeitet und besprochen
Gemeinsame Planung der Wireframes
Aufteilung der HTML-Seiten:
* Julian: index.html
* Diem-Mi: details.html
* Bastian: search.html, add.html
* Jörn: login.html, signup.html  

CSS entsprechend der Aufteilung der HTML-Seiten, die schließlich zusammengeführt wurden  
Gemeinsame Planung und Umsetzung der RESTful API  
Gemeinsame Umsetzung der DTD-Dateien  
Den User haben wir nicht in der REST API implementiert, da wir die User-Logik über Session-Tracking lösen werden.  
 
## Wireframe
[Figma Wireframe](https://www.figma.com/file/a9inGGeGJQBnMfFY8osMQ4/WT?node-id=0%3A1)  

## Restapi
Die Dokumentation für die RestApi finden sie unter folgendem Link:  
[Dokumentation](https://gitlab.com/joern.e/steam_wt/-/blob/dev/doc/openapi.yaml)

## Sitemap
[Index](http://localhost:8080/steam_wt)  
[Login](http://localhost:8080/steam_wt/html/login.html)  
[Sign up](http://localhost:8080/steam_wt/html/signup.html)  
[Search](http://localhost:8080/steam_wt/html/search.html)  
[Details](http://localhost:8080/steam_wt/html/details.html)  
[Add](http://localhost:8080/steam_wt/html/add.html)

## Testing
Initialisierung von Testdaten  
```bash 
curl -i -H “Accept:application/xml” http://localhost:8080/steam_wt/resources/test
```  
Liste von Serien eingeschränkt durch page und items (pagination) ohne Angabe oder bei ungültiger Werte dieser Parameter werden alle Serien ausgeben  
```bash 
curl -i -H “Accept:application/xml” http://localhost:8080/steam_wt/resources/series?page=1&items=2
```   
Gibt das Rating von Basti zu den Simpsons zurück  
```bash 
curl -i -H “Accept:application/xml” http://localhost:8080/steam_wt/resources/rating?title=Simpsons&user=basti`  
```  
Suche nach allen Serien von Basti mit einem score von 3 (good)
```bash   
curl -i -H “Accept:application/xml” http://localhost:8080/steam_wt/resources/series/search?user=basti&score=3
```   
Suche nach allen Serien die “a” enthalten  
```bash 
curl -i -H “Accept:application/xml” http://localhost:8080/steam_wt/resources/series/search?title=a`  
```  
Suche nach allen Serien die “a” enthalten mit Pagination  
```bash 
curl -i -H “Accept:application/xml” http://localhost:8080/steam_wt/resources/series/search?title=a&page=1&items=2  
```  
Anlegen einer Serie  
Dabei wird diese im Moment immer dem User joern hinzugefügt   
```bash 
curl --location --request POST 'http://localhost:8080/steam_wt/resources/series' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "series":
    {
            "numberOfSeasons": 8,
            "title": "Scrubs",
            "streamedBy": "AmazonPrime",
            "genre": "Drama"
    },
    "rating":
    {
        "remark": "Im sick",
        "score": 4
    }
}'
```  

## Sources
[AddIcon](https://www.flaticon.com/free-icon/plus_1828925?term=add&page=1&position=6&page=1&position=6&related_id=1828925&origin=search)  
[SearchIcon](https://www.flaticon.com/free-icon/loupe_622669?term=search&page=1&position=2&page=1&position=2&related_id=622669&origin=search)  
[UserIcon](https://www.flaticon.com/free-icon/user_456212?term=user&page=1&position=7&page=1&position=7&related_id=456212&origin=search)  
[StarIcon](https://www.flaticon.com/free-icon/star_1828961?term=rating&page=1&position=4&page=1&position=4&related_id=1828961&origin=search)  
