/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsh.steam;

import de.hsh.steam.application.Genre;
import de.hsh.steam.application.Score;
import de.hsh.steam.application.Series;
import de.hsh.steam.application.Steamservices;
import de.hsh.steam.application.Streamingprovider;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author pit
 */
@Path("series")
@RequestScoped
public class SeriesCollectionResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of SeriesCollectionResource
     */
    public SeriesCollectionResource() {
    }

    /**
     * Retrieves representation of an instance of de.hsh.steam.SeriesCollectionResource
     * @param user
     * @param page
     * @param items
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public SeriesListModel getXml(
            @DefaultValue("") @QueryParam("user") String user,
            @DefaultValue("0") @QueryParam("page") int page,
            @DefaultValue("0") @QueryParam("items") int items) {
        return getAllSeries(user, page, items);
    }
    
    /**
     * Retrieves representation of an instance of de.hsh.steam.SeriesCollectionResource
     * @param user
     * @param page
     * @param items
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public SeriesListModel getJson(
            @DefaultValue("") @QueryParam("user") String user,
            @DefaultValue("0") @QueryParam("page") int page,
            @DefaultValue("0") @QueryParam("items") int items) {
        return getAllSeries(user, page, items);


    }
    
    private SeriesListModel getAllSeries(String user, int page, int items){
        
        
        ArrayList<Series> series;
                
        if (user.isEmpty()){
            series = Steamservices.singleton().getAllSeries();
        }else{
            series = Steamservices.singleton().getAllSeriesOfUser(user);
        }
        
        
        //TODO sort list alphabetically?
        
        series = Pagination.getPage(series, page, items);

        ArrayList<SeriesModel> allSeriesModel = SeriesModel.convertFromSeries(series);
        
        
        return new SeriesListModel(allSeriesModel);
    }

    /**
     * POST method for creating an instance of SeriesResource
     * @param content representation for the new resource
     * @return an HTTP response with content of the created resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response postXml(RatedSeriesModel content) {
        return addSeries(content);
    }
    
    /**
     * POST method for creating an instance of SeriesResource
     * @param content representation for the new resource
     * @return an HTTP response with content of the created resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postJson(RatedSeriesModel content) {
        return addSeries(content);
    }

    private Response addSeries(RatedSeriesModel content) {
        try {
            
            //TODO user sollte hier der eingeloggte sein
            SeriesModel series = content.getSeries();
            RatingModel rating = content.getRating();
            
            if (!(series.validate() && rating.validate())){
                return Response
                        .status(405)
                        .entity(new ErrorModel("Missing params"))
                        .build();
            }
            
            Steamservices.singleton().addOrModifySeries(
                    series.getTitle(), 
                    series.getNumberOfSeasons(), 
                    series.getGenre(), 
                    series.getStreamedBy(), 
                    "joern", 
                    Score.fromId(rating.getScore()), 
                    rating.getRemark());
            return Response
                    .status(Response.Status.OK)
                    .entity(content)
                    .build();
        } catch (Exception e) { 
            return Response
                    .status(405)
                    .entity(new ErrorModel("Oopsie.."))
                    .build();
        }
    }

    /**
     * Eine bestimmte Serie 
     */
    @Path("{title}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getSeriesXml(@PathParam("title") String title) {
        return getSeries(title);
    }
    
    /**
     * Eine bestimmte Serie
     */
    @Path("{title}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSeriesJson(@PathParam("title") String title) {
        return getSeries(title);
    }
    
    private Response getSeries(String title) {

        Series series = Steamservices.singleton().getSeries(title);
        if (series == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(new ErrorModel(String.format("Series %s was not found", title)))
                    .build();
        }
        
        SeriesModel seriesModel = new SeriesModel(
                series.getTitle(),
                series.getNumberOfSeasons(),
                series.getGenre(), 
                series.getStreamedBy());
        return Response
                .status(Response.Status.OK)
                .entity(seriesModel)
                .build();
    }
    
    /**
     * Sub-resource locator method for {search}
     */
    @Path("search")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response searchSeriesXml(
            @DefaultValue("") @QueryParam("title") String title,
            @DefaultValue("") @QueryParam("genre") String genre,
            @DefaultValue("") @QueryParam("user") String user,
            @DefaultValue("") @QueryParam("streamedBy") String streamedBy,
            @DefaultValue("-1") @QueryParam("score") int score,
            @DefaultValue("0") @QueryParam("page") int page,
            @DefaultValue("0") @QueryParam("items") int items
            ) {
        return search(title, user, genre, streamedBy, score, page, items);
    }
    
    /**
     * Sub-resource locator method for {search}
     */
    @Path("search")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchSeriesJson(
            @DefaultValue("") @QueryParam("title") String title,
            @DefaultValue("") @QueryParam("genre") String genre,
            @DefaultValue("") @QueryParam("user") String user,
            @DefaultValue("") @QueryParam("streamedBy") String streamedBy,
            @DefaultValue("-1") @QueryParam("score") int score,
            @DefaultValue("0") @QueryParam("page") int page,
            @DefaultValue("0") @QueryParam("items") int items) {
        return search(title, user, genre, streamedBy, score, page, items);
    }
    
    private Response search(String title, String user, String genre, String streamedBy, int score, int page, int items) {
        ArrayList<SeriesModel> seriesList;
        SeriesListModel seriesListModel;
        if (!title.isEmpty()) {
            //TODO user muss hier der eingeloggte User sein
            seriesList = SeriesModel.convertFromSeries(Steamservices.singleton().getAllSeriesWithTitle(title));
            
            seriesList = Pagination.getPage(seriesList, page, items);
            seriesListModel = new SeriesListModel(seriesList);
            return Response
                    .status(Response.Status.OK)
                    .entity(seriesListModel)
                    .build();
        }
        
        if (score != -1 & user.isEmpty()){
            return Response
                    .status(405)
                    .entity(new ErrorModel("Missing param 'user' when searching for 'score'"))
                    .build();
        }
        seriesList = SeriesModel.convertFromSeries(
                Steamservices.singleton().search(
                        user.isEmpty() ? null : user,
                        genre.isEmpty() ? null : Genre.valueOf(genre),
                        streamedBy.isEmpty() ? null : Streamingprovider.valueOf(streamedBy),
                        score == (-1) ? null : Score.fromId(score))
                );
        
        seriesList = Pagination.getPage(seriesList, page, items);
        seriesListModel = new SeriesListModel(seriesList);
        return Response
                .status(Response.Status.OK)
                .entity(seriesListModel)
                .build();
    }
}
