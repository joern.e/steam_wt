/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsh.steam;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pit
 */
@XmlRootElement(name = "seriesList")
public class SeriesListModel {
    private ArrayList<SeriesModel> series;

    public SeriesListModel() {
    }

    public SeriesListModel(ArrayList<SeriesModel> series) {
        this.series = series;
    }

    public ArrayList<SeriesModel> getSeries() {
        return series;
    }

    public void setSeries(ArrayList<SeriesModel> series) {
        this.series = series;
    }
}
