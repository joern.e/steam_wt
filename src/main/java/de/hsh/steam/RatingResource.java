/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsh.steam;

import de.hsh.steam.application.Rating;
import de.hsh.steam.application.Steamservices;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Jörn
 */
@Path("rating")
@RequestScoped
public class RatingResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RatingResource
     */
    public RatingResource() {
    }

    /**
     * Retrieves representation of an instance of de.hsh.steam.RatingResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getXml(
            @QueryParam("title") String title, 
            @QueryParam("user") String user) {
        return getRating(title, user);

    }

    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson(
            @QueryParam("title") String title,
            @QueryParam("user") String user) {
        return getRating(title, user);
    }
    
    private Response getRating(String title, String user) {
        try {
            Rating r = Steamservices.singleton().getRating(title, user);
            RatingModel ratingModel = new RatingModel(r);
            return Response
                    .status(Response.Status.OK)
                    .entity(ratingModel)
                    .build();

        } catch (Exception e) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(new ErrorModel(String.format("No Rating found for %s by %s", title, user)))
                    .build();
        }
    }
}
