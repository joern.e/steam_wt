package de.hsh.steam.application;

import java.io.Serializable;

public enum Score implements Serializable {
	
	bad(1), mediocre(2), good(3), very_good(4);
        
        private int id;
        
        private Score(int id) {
            this.id = id;
        }
        
        public int getId() {
            return id;
        }
        
        public static Score fromId(int id) {
                for (Score type : values()) {
                    if (type.getId() == id) {
                        return type;
                    }
                }
                return null;
            }
	
}
