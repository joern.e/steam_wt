/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsh.steam;

import de.hsh.steam.application.Genre;
import de.hsh.steam.application.Series;
import de.hsh.steam.application.Streamingprovider;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pit
 */
@XmlRootElement(name = "series")
@XmlAccessorType(XmlAccessType.FIELD)
public class SeriesModel {
    
    @XmlElement(required=true)
    private String title;
    
    @XmlElement(required=true) 
    private int numberOfSeasons;
    
    @XmlElement(required=true) 
    private Genre genre;
    
    @XmlElement(required=true) 
    private Streamingprovider streamedBy;


    public static ArrayList<SeriesModel> convertFromSeries(ArrayList<Series> series) {
        ArrayList<SeriesModel> seriesModel = new ArrayList<SeriesModel>();
        for (Series s : series) {
           
            seriesModel.add(
                    new SeriesModel(
                            s.getTitle(), 
                            s.getNumberOfSeasons(), 
                            s.getGenre(), 
                            s.getStreamedBy()));
        }
        return seriesModel;
    }
    
    public SeriesModel() {
    }
    
    public boolean validate(){
        return !(title.isEmpty() || title == null || numberOfSeasons < 0 || genre == null || streamedBy == null);
    }

    public SeriesModel(String title, int numberOfSeasons, Genre genre, Streamingprovider streamedBy) {
        this.title = title;
        this.numberOfSeasons = numberOfSeasons;
        this.genre = genre;
        this.streamedBy = streamedBy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNumberOfSeasons() {
        return numberOfSeasons;
    }

    public void setNumberOfSeasons(int numberOfSeasons) {
        this.numberOfSeasons = numberOfSeasons;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Streamingprovider getStreamedBy() {
        return streamedBy;
    }

    public void setStreamedBy(Streamingprovider streamedBy) {
        this.streamedBy = streamedBy;
    }

    
}
