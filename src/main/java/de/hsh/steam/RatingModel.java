/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsh.steam;

import de.hsh.steam.application.Rating;
import javax.json.bind.annotation.JsonbAnnotation;
import javax.json.bind.annotation.JsonbTransient;
import javax.json.bind.annotation.JsonbVisibility;
import javax.json.bind.config.PropertyVisibilityStrategy;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jörn
 */

@XmlRootElement(name = "rating")
@XmlAccessorType(XmlAccessType.FIELD)

public class RatingModel {
    
    @XmlElement(required = true)
    private int score;
    
    @XmlElement(required = true)
    private String remark;

    public RatingModel(){
        
    }
    
    public RatingModel(int score, String remark) {
        this.score = score;
        this.remark = remark;
    }
   
    
    public boolean validate(){
        return !(score < 1 || score > 4 || remark == null);
    }
    
    public RatingModel(Rating r){
        this(r.getScore().getId(), r.getRemark());
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
