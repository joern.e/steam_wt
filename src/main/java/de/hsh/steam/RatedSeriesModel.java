/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsh.steam;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jörn
 */

@XmlRootElement(name = "rating")
@XmlAccessorType(XmlAccessType.FIELD)
public class RatedSeriesModel {
    
    @XmlElement(required = true)
    private SeriesModel series;
    
    @XmlElement(required = true)
    private RatingModel rating;
    
    public RatedSeriesModel(){
        
    }
    
    public RatedSeriesModel(SeriesModel series, RatingModel rating) {
        this.series = series;
        this.rating = rating;
    }
    

    public SeriesModel getSeries() {
        return series;
    }

    public void setSeries(SeriesModel series) {
        this.series = series;
    }

    public RatingModel getRating() {
        return rating;
    }

    public void setRating(RatingModel rating) {
        this.rating = rating;
    }


    
    
}
