/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hsh.steam;

import java.util.ArrayList;

/**
 *
 * @author Jörn
 */
public class Pagination {
    
    public static <T> ArrayList<T> getPage(ArrayList<T> source, int page, int items){
        
        if ((page != 0 && items != 0) && page * items < source.size()) {
            int lastIndex = page * items > source.size()
                    ? source.size() : page * items;
            return new ArrayList<T>(source.subList((page - 1) * items, lastIndex));
        }
        return source;
    }
}
